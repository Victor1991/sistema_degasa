<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {
    public function __construct() {
        parent::__construct();
        $session = $this->auth->is_logged_in();
		if ($session == FALSE) {
             redirect('login');
        }
    }
    
    public function index() {
        $data['errors'] = $this->session->flashdata('errors');
        $data['messages'] = $this->session->flashdata('messages');
        $this->view('admin/dashboard/index', $data);
    }
    
    public function indx_anterior() {
        $this->view('dashboard');
    }
    
}
