<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {
    public function __construct() {
        parent::__construct();
        $session = $this->auth->is_logged_in();
        if ($session == FALSE) {
            redirect('login');
        }

        $this->seccion = '1';
        $this->nombre_seccion = 'Dashboard';
    }

    public function index() {
        $data = '' ;
        $this->view('admin/dashboard/index', $data);
    }



}
